import "cdktf/lib/testing/adapters/jest";
import {listBuckets} from "../Utils";
// import { Testing } from "cdktf";

describe("AWS S3 List Bucket Contents Tests", () => {
  // The tests below are example tests, you can find more information at
  // https://cdk.tf/testing
  it('should check if bucket has Child Objects', async () => {
    let bucketName="local-s3-nginx-upload";

    let response = await listBuckets(bucketName);

    expect(response).toBeDefined();
    expect(response.Contents).toBeDefined();
    expect(response.Contents?.length).toBeGreaterThan(0)


  })


});
