import "cdktf/lib/testing/adapters/jest"; // Load types for expect matchers
import {App} from "cdktf";
import {ConfigProps} from "../lib/ConfigProps";
import {MainStack} from "../main";

describe("Main CDKTF Application", () => {
  // https://cdk.tf/testing
  const props: ConfigProps = {
    ecs:{
      mem:512,
      cpu:256,
      image:"nginx",
      desiredCount:1,
      envMap: {}
    },
    region:"eu-west-2",
    environment:"local",
    cidrBlock:'10.0.0.0/16',
    tags: {
      environment: "local",
    }
  };
  let instance:MainStack;

  beforeAll(() => {
    instance = createTestStack(props);
  })

  const createTestStack = (props:ConfigProps): MainStack => {
    const app = new App();
    return new MainStack(app, 'test', props);
  }

    it("should contain VPC a resource", () => {
      expect(instance.vpcConstruct.vpc.name).toBe(`${props.environment}-VPC`);
    });


    it("should contain an ALB Listener resource", () => {
      expect(instance.albConstruct.getLbListener()).toBeDefined();
      console.log("prt ::"+instance.albConstruct.getLbListener().port)
/*      expect(instance.albConstruct.getLbListener().port).toEqual(80);
      expect(instance.albConstruct.getLbListener().protocol).toEqual("HTTP");*/
    });


    it("should contain an ALB  resource", () => {
      expect(instance.albConstruct.getLb()).toBeDefined();
/*
      expect(instance.albConstruct.getLb().internal).toEqual( false)
*/
    });



    it("should contain an ALB  resource", () => {
      expect(instance.s3Construct.bucket).toBeDefined();
/*
      expect(instance.s3Construct.bucket.bucket).toEqual(`${props.environment}-s3-nginx-upload`)
*/
    });



});
