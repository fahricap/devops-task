import "cdktf/lib/testing/adapters/jest";
import {listTaskDefinitions} from "../Utils";
// import { Testing } from "cdktf";

describe("AWS List TaskDefinition Version Tests", () => {
  // The tests below are example tests, you can find more information at
  // https://cdk.tf/testing
  it('should check if  taskdefinition exists and has versions', async () => {
    let region="eu-west-2";
    let taskFamily="local-ecs-nginx-task-definition";

    let response = await listTaskDefinitions(region, taskFamily);

    expect(response).toBeDefined();
    expect(response.taskDefinitionArns).toBeDefined();
    expect(response.taskDefinitionArns?.length).toBeGreaterThan(0)


  })


});
