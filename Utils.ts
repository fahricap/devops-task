import * as aws from 'aws-sdk';
import {ListObjectsOutput} from "aws-sdk/clients/s3";
import {ListTaskDefinitionsResponse} from "aws-sdk/clients/ecs";

export async function listBuckets (bucketName: string): Promise<ListObjectsOutput>{
    let s3 = new aws.S3();
    let bucketParams = {
        Bucket: bucketName,
    };
    let response= await s3.listObjects(bucketParams).promise();
    return response;
}


export async function listTaskDefinitions(region:string, familyPrefix:string) : Promise<ListTaskDefinitionsResponse>{
    let ecs = new aws.ECS({
        region: region
    });

    let params = {
        familyPrefix: familyPrefix,
    };

    let response=await  ecs.listTaskDefinitions(params).promise();
    return response;
}
