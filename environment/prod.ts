import {ConfigProps} from "../lib/ConfigProps";

export const properties: ConfigProps = {
    ecs:{
        mem:512,
        cpu:1024,
        image:"nginx",
        desiredCount:3,
        envMap: {}
    },
    region:"eu-west-2",
    environment:"prod",
    cidrBlock:'10.0.0.0/16',
    tags: {
        environment: "prod",
    }
};