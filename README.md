# Terraform CDK created test project 


Install Terraform cdk-cli with 
`npm install --global cdktf-cli@latest`

Verify the installation with : `cdktf help`

# Create the CDK project by running the command : 
`cdktf init --template=typescript --local`

# You should get the below output and can use these commands from now on.

Your cdktf typescript project is ready!

cat help                Print this message

Compile:
npm run get           Import/update Terraform providers and modules (you should check-in this directory)
npm run compile       Compile typescript code to javascript (or "npm run watch")
npm run watch         Watch for changes and compile typescript in the background
npm run build         Compile typescript

Synthesize:
cdktf synth [stack]   Synthesize Terraform resources from stacks to cdktf.out/ (ready for 'terraform apply')

Diff:
cdktf diff [stack]    Perform a diff (terraform plan) for the given stack

Deploy:
cdktf deploy [stack]  Deploy the given stack

Destroy:
cdktf destroy [stack] Destroy the stack

Test:
npm run test        Runs unit tests (edit __tests__/main-test.ts to add your own tests)
npm run test:watch  Watches the tests and reruns them on change

Upgrades:
npm run upgrade        Upgrade cdktf modules to latest version
npm run upgrade:next   Upgrade cdktf modules to latest "@next" version (last commit)

Use Prebuilt Providers:

You can find all prebuilt providers on npm: https://www.npmjs.com/search?q=keywords:cdktf
You can install these providers through npm:

npm install @cdktf/provider-aws
npm install @cdktf/provider-google
npm install @cdktf/provider-azurerm
npm install @cdktf/provider-docker
npm install @cdktf/provider-github
npm install @cdktf/provider-null

You can also build any module or provider locally. Learn more https://cdk.tf/modules-and-providers



## You can also deploy using the deploy.sh by running `sh deploy.sh` which runs all required commands to deploy the stack.
## To deploy to a specific environment, you need to set the environment variable `AWS_ENV_NAME` before running the script, to use the chosen environment. ex : export AWS_ENV_NAME=dev  

# Utility Scripts 
- s3 list script should be run with `node listS3.js {BUCKET_NAME_TO_LIST_CONTENTS}`, with a single program argument.It should list all objects (folders+files) in the folder. 
  - Sample Command: `node listS3.js local-s3-nginx-upload`
- Tasdefinition list should be run with `node listTaskDefinitions.js {REGION} {FAMILY}`, with two arguments,  REGION and TASK_FAMILY_NAME.  
  - Sample Command:  `node listTaskDefinitions.js eu-west-2 local-ecs-nginx-task-definition`



# References 
- https://faun.pub/getting-started-with-cdktf-f9a1f3ed228c
- https://github.com/hashicorp/docker-on-aws-ecs-with-terraform-cdk-using-typescript/tree/main/infrastructure
- https://github.com/hashicorp/terraform-cdk/blob/main/examples/typescript/aws-multiple-stacks/main.ts
- https://learn.hashicorp.com/tutorials/terraform/cdktf-applications
- https://learn.hashicorp.com/tutorials/terraform/cdktf-install
