import {Lb, LbListener,} from "@cdktf/provider-aws/lib/elb";
import {Fn, Resource,} from "cdktf";

import {Construct} from "constructs";
import {ConfigProps} from "./ConfigProps";
import {SecurityGroup} from "@cdktf/provider-aws/lib/vpc";
import {Vpc} from "../.gen/modules/terraform-aws-modules/aws/vpc";


export class ALBConstruct extends Resource {
    private lb: Lb;
    private lbl: LbListener;
    private lbSecurityGroup: SecurityGroup;

    constructor(
        scope: Construct,
        id: string,
        props: ConfigProps,
        vpc: Vpc
    ) {
        super(scope, id);
        let env = props.environment;
        let name = `${env}-alb`

        this.lbSecurityGroup = new SecurityGroup(this, `${env}-alb-security-group`, {
            vpcId: Fn.tostring(vpc.vpcIdOutput),
            tags: props.tags,
            name: `${env}-alb-security-group`,
            ingress: [
                // allow only HTTP traffic  (This should be integrated with Route53 and converted to HTTPS )
                {
                    protocol: "TCP",
                    fromPort: 80,
                    toPort: 80,
                    cidrBlocks: ["0.0.0.0/0"],
                    ipv6CidrBlocks: ["::/0"],
                },
            ],
            egress: [
                // allow all traffic to outside world
                {
                    fromPort: 0,
                    toPort: 0,
                    protocol: "-1",
                    cidrBlocks: ["0.0.0.0/0"],
                    ipv6CidrBlocks: ["::/0"],
                },
            ],
        });


        this.lb = new Lb(this, name, {
            name: name,
            tags: props.tags,
            internal: false,
            loadBalancerType: "application",
            securityGroups: [this.lbSecurityGroup.id],

        });


        this.lb.addOverride("subnets", vpc.publicSubnetsOutput);

        this.lbl = new LbListener(this, `lb-listener`, {
            loadBalancerArn: this.lb.arn,
            port: 80,
            protocol: "HTTP",
            tags: props.tags,
            defaultAction: [
                // We define a fixed 404 message, just in case
                {
                    type: "fixed-response",
                    fixedResponse: {
                        contentType: "text/plain",
                        statusCode: "404",
                        messageBody: "Could not find the resource you are looking for",
                    },
                },
            ],
        });


    }

    public getLbSecurityGroup(): SecurityGroup {
        return this.lbSecurityGroup;
    }

    public getLb(): Lb {
        return this.lb;
    }

    public getLbListener(): LbListener {
        return this.lbl;
    }


}
