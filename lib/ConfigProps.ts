export class ConfigProps {
    cidrBlock: string;
    environment: string;
    ecs: {
        cpu: number;
        mem: number;
        image: string;
        desiredCount: number;
        envMap: { [key: string]: string }
    };
    region: string;
    tags: { [key: string]: string };
}