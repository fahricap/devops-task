import {EcsCluster, EcsClusterCapacityProviders, EcsService, EcsTaskDefinition,} from "@cdktf/provider-aws/lib/ecs";


import {Construct} from "constructs";
import {ConfigProps} from "./ConfigProps";
import {Fn, Resource} from "cdktf";
import {LbListenerRule, LbTargetGroup} from "@cdktf/provider-aws/lib/elb";
import {Vpc} from "../.gen/modules/terraform-aws-modules/aws/vpc";
import {CloudwatchLogGroup} from "../.gen/providers/aws/cloudwatch";
import {IAMConstruct} from "./iam-construct";
import {ALBConstruct} from "./alb-construct";

export class EcsConstruct extends Resource {
    public cluster: EcsCluster;
    public props: ConfigProps;
    public env: string;
    vpc: Vpc;
    albConstruct: ALBConstruct;
    private iamConstruct: IAMConstruct;

    constructor(
        scope: Construct,
        id: string,
        props: ConfigProps,
        vpc: Vpc,
        albConstruct: ALBConstruct,
        iamConstruct: IAMConstruct,
    ) {
        super(scope, id);
        this.props = props;
        this.env = props.environment;
        this.vpc = vpc;
        this.iamConstruct = iamConstruct;
        this.albConstruct = albConstruct;
        this.createEcsCluster();
        let task = this.createECSTask();
        this.createAndExposeService(task)
    }

    createAndExposeService(
        task: EcsTaskDefinition,
    ) {
        let name = `${this.props.environment}-nginx`
        // Define Load Balancer target group with a health check on /ready
        const targetGroup = new LbTargetGroup(this, `${name}-tg`, {
            dependsOn: [this.albConstruct.getLbListener()],
            tags: this.props.tags,
            name: `${name}-tg`,
            port: 80,
            protocol: "HTTP",
            targetType: "ip",
            vpcId: Fn.tostring(this.vpc.vpcIdOutput),
            healthCheck: {
                enabled: true,
                path: "/",
            },
        });

        new LbListenerRule(this, `rule`, {
            listenerArn: this.albConstruct.getLbListener().arn,
            priority: 100,
            tags: this.props.tags,
            action: [
                {
                    type: "forward",
                    targetGroupArn: targetGroup.arn,
                },
            ],

            condition: [
                {
                    pathPattern: {values: [`*`]},
                },
            ],
        });

        // Ensure the task is running and wired to the target group, within the right security group
        new EcsService(this, `${this.props.environment}- ${name}-service`, {
            dependsOn: [this.albConstruct.getLbListener()],
            tags: this.props.tags,
            name: `${name}-service`,
            launchType: "FARGATE",
            cluster: this.cluster.id,
            desiredCount: this.props.ecs.desiredCount,
            taskDefinition: task.arn,
            networkConfiguration: {
                subnets: Fn.tolist(this.vpc.publicSubnetsOutput),
                assignPublicIp: true,
                securityGroups: [this.albConstruct.getLbSecurityGroup().id],
            },
            loadBalancer: [
                {
                    containerPort: 80,
                    containerName: `${this.props.environment}-nginx`,
                    targetGroupArn: targetGroup.arn,
                },
            ],
        });
    }

    private createEcsCluster(): EcsCluster {
        // Create bucket
        let clusterName = `${this.env}-ecs-cluster`

        const cluster = new EcsCluster(this, `ecs-${clusterName}`, {
            name: clusterName,
            tags: this.props.tags,
        });

        new EcsClusterCapacityProviders(this, `capacity-providers-${clusterName}`, {
            clusterName: cluster.name,
            capacityProviders: ["FARGATE"],
        });

        this.cluster = cluster;

        return cluster;
    }

    private createECSTask(): EcsTaskDefinition {
        // Creates a task that runs the docker container
        let name = `${this.env}-ecs-nginx-task-definition`;
        let logGroupName = `${this.env}-log-group`
        const logGroup = new CloudwatchLogGroup(this, logGroupName, {
            name: logGroupName,
            retentionInDays: 30,
            tags: this.props.tags,
        });

        const taskDefinition = new EcsTaskDefinition(this, name, {
            tags: this.props.tags,
            cpu: `${this.props.ecs.cpu}`,
            memory: `${this.props.ecs.mem}`,
            requiresCompatibilities: ["FARGATE"],
            networkMode: "awsvpc",
            executionRoleArn: this.iamConstruct.getExecutionRole().arn,
            taskRoleArn: this.iamConstruct.getTaskRole().arn,
            containerDefinitions: JSON.stringify([
                {
                    name: `${this.props.environment}-nginx`,
                    image: this.props.ecs.image,
                    cpu: this.props.ecs.cpu,
                    memory: this.props.ecs.mem,
                    environment: Object.entries(this.props.ecs.envMap).map(([name, value]) => ({
                        name,
                        value,
                    })),
                    portMappings: [
                        {
                            containerPort: 80,
                            hostPort: 80,
                        },
                    ],
                    logConfiguration: {
                        logDriver: "awslogs",
                        options: {
                            // Defines the log
                            "awslogs-group": logGroup.name,
                            "awslogs-region": this.props.region,
                            "awslogs-stream-prefix": name,
                        },
                    },
                },
            ]),
            family: name,
        });

        return taskDefinition;
    }

}
