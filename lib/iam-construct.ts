import {Construct} from "constructs";
import {ConfigProps} from "./ConfigProps";
import {Resource} from "cdktf";
import {IamRole} from "@cdktf/provider-aws/lib/iam";

export class IAMConstruct extends Resource {
    private props: ConfigProps;
    private executionRole: IamRole;
    private taskRole: IamRole;

    constructor(
        scope: Construct,
        id: string,
        props: ConfigProps
    ) {
        super(scope, id);
        this.props = props;

        let env = props.environment;
        let name = `${env}-nginx-ecs`;


        this.executionRole = new IamRole(this, `execution-role`, {
            name: `${name}-execution-role`,
            tags: this.props.tags,
            inlinePolicy: [
                {
                    name: `${env}-ecs-cloudwatch-access`,
                    policy: JSON.stringify({
                        Version: "2012-10-17",
                        Statement: [
                            {
                                Effect: "Allow",
                                Action: [
                                    "ecr:GetAuthorizationToken",
                                    "ecr:BatchCheckLayerAvailability",
                                    "ecr:GetDownloadUrlForLayer",
                                    "ecr:BatchGetImage",
                                    "logs:CreateLogStream",
                                    "logs:PutLogEvents",
                                ],
                                Resource: "*",
                            },
                        ],
                    }),
                }
            ],
            // this role shall only be used by an ECS task
            assumeRolePolicy: JSON.stringify({
                Version: "2012-10-17",
                Statement: [
                    {
                        Action: "sts:AssumeRole",
                        Effect: "Allow",
                        Sid: "",
                        Principal: {
                            Service: "ecs-tasks.amazonaws.com",
                        },
                    },
                ],
            }),
        });

        // Role that allows us to push logs
        this.taskRole = new IamRole(this, `task-role`, {
            name: `${name}-task-role`,
            tags: this.props.tags,
            inlinePolicy: [
                {
                    name: "allow-logs",
                    policy: JSON.stringify({
                        Version: "2012-10-17",
                        Statement: [
                            {
                                Effect: "Allow",
                                Action: ["logs:CreateLogStream", "logs:PutLogEvents"],
                                Resource: "*",
                            },
                        ],
                    }),
                },
                {
                    name: `${env}-allow-s3-access`,
                    policy: JSON.stringify({
                        Version: "2012-10-17",
                        Statement: [
                            {
                                Effect: "Allow",
                                Action: [
                                    "s3:*"
                                ],
                                Resource: `arn:aws:s3:::${env}-s3-nginx-upload/*`,
                            },
                        ],
                    }),
                },
            ],
            assumeRolePolicy: JSON.stringify({
                Version: "2012-10-17",
                Statement: [
                    {
                        Action: "sts:AssumeRole",
                        Effect: "Allow",
                        Sid: "",
                        Principal: {
                            Service: "ecs-tasks.amazonaws.com",
                        },
                    },
                ],
            }),
        });
    }

    public getExecutionRole(): IamRole {
        return this.executionRole;
    }

    public getTaskRole(): IamRole {
        return this.taskRole;
    }


}
