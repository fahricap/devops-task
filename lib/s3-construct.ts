import {S3Bucket,} from "@cdktf/provider-aws/lib/s3";

import {Construct} from "constructs";
import {ConfigProps} from "./ConfigProps";
import {Resource} from "cdktf";

export class S3Construct extends Resource {
    bucket: S3Bucket;
    private props: ConfigProps;

    constructor(
        scope: Construct,
        id: string,
        props: ConfigProps
    ) {
        super(scope, id);
        this.props = props;

        this.bucket=this.createS3();

    }


    private createS3(): S3Bucket {
        // Create bucket
        let name = `${this.props.environment}-s3-nginx-upload`
        let bucket = new S3Bucket(this, name, {
            bucket: name,
            tags: this.props.tags,
        });


        return bucket;
    }
}
