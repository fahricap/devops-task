import {Vpc} from "../.gen/modules/terraform-aws-modules/aws/vpc";
//import { AwsProvider } from '../.gen/providers/aws'
import {Construct} from "constructs";
import {ConfigProps} from "./ConfigProps";
import {Resource} from "cdktf";

export class VpcConstruct extends Resource {
    private props: ConfigProps;

    constructor(
        scope: Construct,
        id: string,
        props: ConfigProps
    ) {
        super(scope, id);
        this.props = props;
        this._vpc = this.createVPC();

    }

    private _vpc: Vpc;

    get vpc(): Vpc {
        return this._vpc;
    }

    private createVPC(): Vpc {

        let vpc = new Vpc(this, `${this.props.environment}-vpc`, {
            cidr: '10.0.0.0/16',
            azs: ['eu-west-2a', 'eu-west-2b', 'eu-west-2c'],
            publicSubnets: ['10.0.10.0/24', '10.0.20.0/24'],
            privateSubnets: ['10.0.30.0/24', '10.0.40.0/24'],
            enableNatGateway: true,
            tags: this.props.tags,
            name: `${this.props.environment}-VPC`,
            singleNatGateway: true,
        });

        return vpc;


    }
}
