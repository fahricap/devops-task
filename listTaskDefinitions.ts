import {listTaskDefinitions} from "./Utils";


let REGION = `eu-west-2`;
let DEFINITION_NAME = ``;
const args = process.argv;

if (args && args.length>2) {
    REGION=args[2];
    DEFINITION_NAME=args[3];
}


listTaskDefinitions(REGION, DEFINITION_NAME).then(data=> {
    if (!data) {
        console.log("Error", data);
    } else {
        let datas = data.taskDefinitionArns;
        if (datas && datas.length) {
            console.log("Taskdefinition List :\n");
            for (let da of datas) {
                console.log(`${da}\n`);
            }
        } else {
            console.log("No definition found :\n");
        }

    }
});
