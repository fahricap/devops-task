import {Construct} from "constructs";
import {App, TerraformStack} from "cdktf";
import {AwsProvider} from './.gen/providers/aws'
import {ConfigProps} from "./lib/ConfigProps";
import {VpcConstruct} from "./lib/vpc-construct";
import {S3Construct} from "./lib/s3-construct";
import {ALBConstruct} from "./lib/alb-construct";
import {IAMConstruct} from "./lib/iam-construct";
import {EcsConstruct} from "./lib/ecs-construct";


export class MainStack extends TerraformStack {
    public albConstruct: ALBConstruct;
    public vpcConstruct: VpcConstruct;
    public iamConstruct: IAMConstruct;
    public ecsConstruct: EcsConstruct;
    public s3Construct: S3Construct;
    constructor(scope: Construct, name: string, props: ConfigProps) {
        // define resources here
        super(scope, name);
        new AwsProvider(this, 'aws', {
            region: props.region
        });

        const env = props.environment;
        this.vpcConstruct = new VpcConstruct(this, `${env}-vpc-resource`, props);
        let vpc = this.vpcConstruct.vpc;
        this.albConstruct = new ALBConstruct(this, `${env}-alb-resource`, props, vpc);
        this.iamConstruct = new IAMConstruct(this, `${env}-iam-resource`, props);
        this.ecsConstruct = new EcsConstruct(this, `${env}-ecs-resource`, props, vpc, this.albConstruct, this.iamConstruct);
        this.s3Construct = new S3Construct(this, `${env}-s3-resource`, props);

    }
}


const env = process.env.AWS_ENV_NAME || "local"; //local, dev, staging,prod

const propsFile = import(`./environment/${env}.ts`);
propsFile.then((properties) => {
    let props = properties.properties;
    if (props.environment == "") {
        props.environment = env;
    }

    const app = new App();
    new MainStack(app, "devops-task", props);
    app.synth();

});
