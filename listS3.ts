import {listBuckets} from "./Utils";

let BUCKET_NAME = `local-s3-nginx-upload`;
const args = process.argv;
if (args && args.length > 2) {
    BUCKET_NAME = args[2];
} else {
    console.log("Please enter a Bucket name to list");
    process.exit(1);
}


listBuckets(BUCKET_NAME).then(data=> {
    if (!data) {
        console.log("Error", data);
    } else {
        let datas = data.Contents;
        if (datas && datas.length) {
            console.log("Bucket contents :\n");
            console.log(`Name\t LastModified\t StorageClass \t Size `);
            console.log("---------------------------------------------------------------------------------------------\n");
            for (let da of datas) {
                console.log(`${da.Key}\t ${da.LastModified}\t ${da.StorageClass} \t ${da.Size} `);
            }
        } else {
            console.log("Bucket is Empty :\n");
        }

    }

});


